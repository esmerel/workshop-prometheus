<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="Prometheus workshop">
		<meta name="author" content="Eric D. Schabell">

		<title>Lab 3 - Prometheus</title>

		<link rel="stylesheet" href="dist/reset.css">
		<link rel="stylesheet" href="dist/reveal.css">
		<link rel="stylesheet" href="dist/theme/chrono.css">

		<!-- Theme used for syntax highlighted code -->
		<link rel="stylesheet" href="plugin/highlight/monokai.css">
	</head>
	<body>
		<div class="reveal">
			<div class="slides">
				<section>
					<h3 class="r-fit-text">Lab 3 - Introduction to the Query Language</h3>
				</section>
				<section>
					<div style="width: 1056px; height: 250px;">
						<h2>Lab Goal</h2>
						<h4>This lab introduces the Prometheus Query Language (PromQL) giving you an introduction
							and sets up a demo project to provide more realistic data for querying.</h4>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 130px;">
						<h3 class="r-fit-text">Prometheus - What query language?</h3>
					</div>
					<div style="width: 1056px; height: 450px; text-align: left; font-size: xx-large;">
						A query language is needed by Prometheus to be able to send requests to the stored metrics
						data, allowing users to gain ad-hoc insights, build visualizations and dashboards from this
						data, and be able to report (alert) when incoming data indicates that systems are not performing
						as desired.<br />
						<br />
						This language is called PromQL and provides an open standard unified way of selecting,
						aggregating, transforming, and computing on the collected time series data. Note that this
						language provides only READ access to the collected metrics data, while Prometheus offers a
						different path to WRITE access.
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 130px;">
						<h3 class="r-fit-text">PromQL - Proving PromQL compliance?</h3>
					</div>
					<div style="width: 1056px; height: 400px; text-align: left; font-size: xx-large;">
						PromQL is an open standard in that it's widely integrated by many vendors in their products,
						which begs the question, how can I be sure that a product is 100% compatible with the real
						open source PromQL found in the Prometheus project?<br />
						<br />
						To answer this question, the
						<a href="https://github.com/prometheus/compliance/blob/main/promql/README.md" target="_blank">
							PromQL Compliance Tester
						</a>
						was added to the larger
						<a href="https://github.com/prometheus/compliance" target="_blank">
							Prometheus compliance project.
						</a>
						Follow the documentation and you can test any vendor you like, or you can browse one of the
						<a href="https://promlabs.com/promql-compliance-test-results/2021-10-14/chronosphere" target="_blank">
							formatted results published online.
						</a>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">PromQL - Compliance testing results</h3>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: xx-large;">
						You can see here that
						<a href="https://Chronosphere.io?utm_source=schabell-blog&utm_medium=eric" target="_blank">Chronosphere</a>
						has full compliance with the Prometheus Query Language and you can view the entire output of the
						<a href="https://promlabs.com/promql-compliance-tests" target="_blank">compliance tests online</a>:
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab03-1.png" alt="compliance"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">PromQL - Prometheus architecture</h3>
					</div>
					<div style="width: 1056px; height: 100px; text-align: left; font-size: xx-large;">
						Remember the overview architecture of Prometheus as it was presented in the first introduction?
						The next slide will expose the Prometheus query engine:
					</div>
					<div style="width: 1056px; height: 500px;">
						<img src="images/lab03-2.png" alt="compliance"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">PromQL - Query engine architecture</h3>
					</div>
					<div style="width: 1056px; height: 140px; text-align: left; font-size: x-large;">
						Taking a closer look here at our Prometheus internals, we find the ingested time series data
						(metrics) are scraped from configured targets and stored in the TSDB. An internal PromQL engine
						supports our ability to query that data. All queries are in read-only access. The query
						engine supports both <b>internal</b> and <b>external</b> queries. Let's take a look at some
						rule terminology before we dig any further:
					</div>
					<div style="width: 1056px; height: 450px;">
						<img src="images/lab03-3.png" alt="compliance"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 130px;">
						<h3 class="r-fit-text">Intermezzo - Defining queries and rules</h3>
					</div>
					<div style="width: 1056px; height: 400px; text-align: left; font-size: xx-large;">
						Before we get too deep into PromQL, let's look closer at the various rule terminology you'll
						be covering. First a query and rule:<br />
						<br />
						<ul>
							<li>
								<b>Query</b> - a PromQL query is not like SQL (SELECT * FROM...), but consist of nested
								functions with each inner function returning the data described to the next outer function.
							</li>
							<br />
							<li>
								<b>Rule</b> - a configured query to gather data and evaluate, either as a
								<b>recording rule</b> or an <b>alerting rule</b>.
							</li>
						</ul>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 130px;">
						<h3 class="r-fit-text">Intermezzo - Recording and alerting rules</h3>
					</div>
					<div style="width: 1056px; height: 400px; text-align: left; font-size: xx-large;">
						Next the recording rule and alerting rule, essential to complexer actions:<br />
						<br />
						<ul>
							<li>
								<b>Recording rule</b> - used to pre-query often used data or computationally expensive
								expressions and save the results for faster execution of queries later. Useful for queries
								used in dashboards (refreshed often).
							</li>
							<br />
							<li>
								<b>Alerting rule</b> - defines an alert condition based on PromQL expressions, when fired
								cause notifications to be sent to external services.
							</li>
						</ul>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 130px;">
						<h3 class="r-fit-text">Intermezzo - Aggregation and filtering</h3>
					</div>
					<div style="width: 1056px; height: 400px; text-align: left; font-size: xx-large;">
						Finally, a look at aggregation and filtering, very important to optimizing both execution as
						well as trimming excessive unused data metrics:<br />
						<br />
						<ul>
							<li>
								<b>Aggregation</b> - using operators that support combining elements from a single function,
								resulting in a new results of fewer elements with combined values (SUM, MIN, MAX, AVG...)
							</li>
							<br />
							<li>
								<b>Filtering</b> - the act of removing metrics from a query result by exclusion,
								aggregation, or applying language functions to reduce the results.
							</li>
						</ul>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">PromQL - Prometheus internal queries</h3>
					</div>
					<div style="width: 1056px; height: 100px; text-align: left; font-size: x-large;">
						Now to how internal queries to Prometheus run. Recording and alerting rules are executed on a
						regular schedule to calculate rule results, such as an alert needing to fire. As you configure
						new rules, these activities happen automatically:
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab03-4.png" alt="compliance"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">PromQL - The external queries</h3>
					</div>
					<div style="width: 1056px; height: 100px; text-align: left; font-size: x-large;">
						Queries can be sent externally to Prometheus using the
						<a href="https://prometheus.io/docs/prometheus/latest/querying/api/" target="_blank">
							Prometheus API (HTTP).
						</a>
						External users, user interfaces (UIs), and dashboards are all examples of querying Prometheus
						metrics using PromQL. This is also how Prometheus uses it's built-in web console to runs queries:
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab03-5.png" alt="compliance"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">PromQL - Exploring a few use cases</h3>
					</div>
					<div style="width: 1056px; height: 400px; text-align: left; font-size: xx-large;">
						While there are many use cases that PromQL can support, it's possible to group them in to a few
						more general ones that are more common in your daily observability work:<br />
						<br />
						<ul>
							<li>Ad-hoc querying</li>
							<li>Dashboards</li>
							<li>Alerting</li>
							<li>Automation</li>
						</ul>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">PromQL use cases - Ad-hoc querying</h3>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: x-large;">
						This use case is about you running live queries against the collected time series data. Imagine
						you are getting alerts while on call at your organization, you open the dashboard and the
						pre-configured display gives you some hints as to the issue but you want to dig specifically
						into some data points. That's when you write your own ad-hoc query and execute it to view the
						data in a graph:
					</div>
					<div style="width: 1056px; height: 430px;">
						<img src="images/lab03-6.png" alt="ad-hoc query"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">PromQL use cases - Dashboard queries</h3>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: x-large;">
						This use case is where you create a layout of queries in what is know as a dashboard. You design
						your display of metrics, gauges, and charts you want to display for a specific user viewing
						aspects of your systems. PromQL queries are used to collect data, here using the
						<a href="https://github.com/perses/perses" target="_blank">Perses project</a> (you'll learn
						about dashboards later in this workshop) and embedded it in a dashboard view:
					</div>
					<div style="width: 1056px; height: 430px;">
						<img src="images/lab03-7.png" alt="ad-hoc query"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">PromQL use cases - Alerting queries</h3>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: x-large;">
						The use of queries to watch your collected data for possible alerts is another use case. Prometheus
						generates alerts based on queries such as this one looking for hardware failure:
					</div>
					<div style="width: 1056px; height: 430px;">
						<pre>
							<code data-trim data-noescape>
								groups:
								- name: Hardware alerts
									rules:
									- alert: Node down
									expr: up{job="node_exporter"} == 0
									for: 3m
									labels:
										severity: warning
									annotations:
										title: Node {{ $labels.instance }} is down
										description: No scrape {{ $labels.job }} on {{ $labels.instance }}.
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">PromQL use cases - Dispatching alerts</h3>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: x-large;">
						To make these alerts useful, you might want to dispatch them to Slack, PagerDuty, or some
						other notification mechanism. Here is an example of what Slack might look like when you
						dispatch an alert notification:
					</div>
					<div style="width: 1056px; height: 200px;">
						<img src="images/lab03-8.png" alt="ad-hoc query"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">PromQL use cases - Query automation</h3>
					</div>
					<div style="width: 1056px; height: 300px; text-align: left; font-size: x-large;">
						When you are automating your processes you can run PromQL queries against Prometheus collected
						data and make choices based on the results. A few examples you might consider:<br />
						<br />
						<ul>
							<li>In your CI/CD pipeline, inspecting a deployment stage health before full deployment.</li>
							<li>Kicking off a remediation process when a system alerts to a deteriorated state.</li>
							<li>Autoscaling to provision more infrastructure when increased load detected.</li>
						</ul>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Query architecture</h3>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: x-large;">
						That's enough theory about queries for now, let's look at installing and running a services demo
						project (source: <a href="https://github.com/juliusv/prometheus_demo_service" target="_blank">
						with thanks to this repository</a>) that will allow you to query somewhat realistic scraped services
						time series data. The architecture is simple:
					</div>
					<div style="width: 1056px; height: 400px;">
						<img src="images/lab03-9.png" alt="services architecture"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Metrics being generated</h3>
					</div>
					<div style="width: 1056px; height: 350px; text-align: left; font-size: xx-large;">
						The services demo architecture shows the layout, but what are these services providing for
						our Prometheus instance to collect metrics from? It's exporting synthetic metrics (specifically
						designed metrics) about our simulated services, here's a few examples:<br />
						<br />
						<ul>
							<li>HTTP API server exposing request counts and latencies</li>
							<li>Periodic batch job exposing timestamp and number of processed bytes</li>
							<li>Metrics: CPU usage, memory usage, size of disk, disk usage, and more</li>
						</ul>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Installing and running</h3>
					</div>
					<div style="width: 1056px; height: 350px; text-align: left; font-size: xx-large;">
						To install all of this, we're providing a self contained project you can download, unzip,
						and run an installation script to quickly get started following these easy steps:<br />
						<br />
						<ol>
							<li><a href="https://gitlab.com/o11y-workshops/prometheus-service-demo-installer/-/archive/main/prometheus-service-demo-installer-main.zip" target="_blank">Download and unzip the Prometheus Services Demo Installer.</a></li>
							<li>Run 'init.sh' file.</li>
							<li>Follow the instructions in the terminal output to start the services.</li>
						</ol>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Download and unzip</h3>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: x-large;">
						Once you have downloaded the project using the link provided previously, just unzip into its
						own directory as shown (Note: examples shown are on Mac OSX system):
					</div>
					<div style="width: 1056px; height: 450px;">
						<img src="images/lab03-10.png" alt="unzip services"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Exploring the installer</h3>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: x-large;">
						Now change into the project directory explore the demo source code found in the install
						directory as show. This is the code that the installer will build automatically:
					</div>
					<div style="width: 1056px; height: 450px;">
						<img src="images/lab03-11.png" alt="explore sources"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Run installation script</h3>
					</div>
					<div style="width: 1056px; height: 350px; text-align: left; font-size: xx-large;">
						Time to build this project, so let's run the init script to build the services
						project. Run the command below and see the next slide for the output and explanation:<br />
						<pre data-trim data-noescape>
							<code>$ ./init.sh</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Installation introduction</h3>
					</div>
					<div style="width: 1056px; height: 40px; text-align: left; font-size: xx-large;">
						The installation starts off with a nice ascii art sign to let you know you are about to install
						the services project:<br />
					</div>
					<div style="width: 1056px; height: 550px; text-align: right;">
						<img src="images/lab03-12.png" alt="ascii art"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Testing and building</h3>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: xx-large;">
						Next the installer tests if you have GO installed and a valid version, if not you are pointed
						to where to install it. In this example you see that it's a valid version of Go so it continues
						on to build the services demo code:<br />
					</div>
					<div style="width: 1056px; height: 250px;">
						<img src="images/lab03-13.png" alt="test and build"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Completed building</h3>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: xx-large;">
						If all goes well you have now see the following successful build message with pointers to how
						to run the services demo binary you built and have the link to verify the metrics are exposed
						in your browser:<br />
					</div>
					<div style="width: 1056px; height: 350px;">
						<img src="images/lab03-14.png" alt="completed build"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Testing the metrics</h3>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: xx-large;">
						After starting the services binary as shown in the terminal output, it should have metrics
						scraping end points available so that you can query them with PromQL. Test this at
						<code>http://localhost:8080/metrics</code>:
					</div>
					<div style="width: 1056px; height: 350px;">
						<img src="images/lab03-15.png" alt="services metrics"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Prometheus configuration</h3>
					</div>
					<div style="width: 1056px; height: 300px; text-align: left; font-size: xx-large;">
						While the metrics are exposed, they will not be scraped by Prometheus until you have updated its
						configuration to add this new end point. Let's update our <code>workshop-prometheus.yml</code>
						file to add the services job as shown along with comments for clarity:<br />
						<pre>
							<code data-trim data-noescape>
								scrape_configs:

									# Scraping Prometheus.
									- job_name: "prometheus"
									static_configs:
										- targets: ["localhost:9090"]

									# Scraping services demo.
									- job_name: "services"
									static_configs:
										targets: ["localhost:8080"]
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h3 class="r-fit-text">Services demo - Applying the configuration</h3>
					</div>
					<div style="width: 1056px; height: 200px; text-align: left; font-size: xx-large;">
						Previously you learned to restart the Prometheus to apply configuration changes. If you have
						a running instance you don't want to lose a period of collecting time series data, so let's send
						a restart signal instead using the <code>kill</code> command. First we find the Prometheus server
						process id (PID) using one command, then apply it using the command as shown below:
					</div>
					<div style="width: 900px; height: 200px; text-align: left; font-size: xx-large">
						<pre>
							<code data-trim data-noescape>
								# Locate the Prometheus process id (PID).
								#
								$ ps aux  | grep prometheus

								erics     94110   1:31PM   0:05.28 ./prometheus --config.file=workshop-prometheus.yml
								erics     97648   2:43PM   0:00.00 grep prometheus

								# Send a restart signal to the PID we found.
								#
								$ kill -s HUP 94110
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h3 class="r-fit-text">Intermezzo - Support helper configuration file</h3>
					</div>
					<div style="width: 1056px; height: 200px; text-align: left; font-size: xx-large;">
						The previous Prometheus configuration changes were applied by you to your workshop file. In
						case you want to return to this workshop at a later time, we provide a working adjusted
						configuration file in the support director, just start Prometheus as follows to start scraping
						both Prometheus and the service demo endpoints:
					</div>
					<div style="width: 900px; height: 200px; text-align: left; font-size: xxx-large">
						<pre>
							<code data-trim data-noescape>
								$ ./prometheus --config.file=support/workshop-prometheus.yml
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Configuration applied</h3>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: xx-large;">
						No matter which method you used to apply the new Prometheus configuration, validate it by
						checking the log output, you should see something like this (example of using a signal to
						restart):
					</div>
					<div style="width: 900px; height: 300px;">
						<img src="images/lab03-16.png" alt="services metrics"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h3 class="r-fit-text">Services demo - Validating setup</h3>
					</div>
					<div style="width: 1056px; height: 100px; text-align: left; font-size: x-large;">
						The last check to make sure it's working, execute <b><code>demo_api_http_requests_in_progress</code></b>
						in the Prometheus console to generate a graph output something like the following (note, this has
						been running awhile, hence the full graph):
					</div>
					<div style="width: 900px; height: 500px;">
						<img src="images/lab03-17.png" alt="services metrics"><br />
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 150px;">
						<h2 class="r-fit-text">Lab completed - Results</h2>
					</div>
					<div style="width: 1056px; height: 400px; font-size: xx-large">
						<img src="images/lab03-17.png" alt="completed">
						<br />
						Next up, exploring basic queries...
					</div>
				</section>

				<section data-background="images/questions.png">
					<span class="menu-title" style="display: none">References</span>
					<div style="width: 1056px; height: 200px;">
						<img height="150" width="100%" src="images/references.jpg" alt="references">
					</div>
					<div style="width: 1056px; height: 400px; font-size: xx-large; text-align: left">
						<ul>
							<li><a href="https://o11y-workshops.gitlab.io/" target="_blank">Getting started with cloud native o11y workshops</a></li>
							<li><a href="https://gitlab.com/o11y-workshops/workshop-prometheus" target="_blank">This workshop project repository</a></li>
							<li><a href="https://gitlab.com/o11y-workshops" target="_blank">O11y workshop collection</a></li>
							<li><a href="https://prometheus.io" target="_blank">Prometheus project</a></li>
							<li><a href="https://github.com/prometheus/promlens" target="_blank">PromLens project</a></li>
							<li><a href="https://github.com/perses/perses" target="_blank">Perses project</a></li>
							<li><a href="https://gitlab.com/o11y-workshops/workshop-prometheus/-/issues/new" target="_blank">Report an issue with this workshop</a></li>
						</ul>
					</div>
				</section>

				<section>
					<span class="menu-title" style="display: none">Questions or feedback?</span>
					<div style="width: 1056px; height: 150px">
						<h2 class="r-fit-text">Contact - are there any questions?</h2>
					</div>
					<div style="width: 1056px; height: 200px; font-size: x-large; text-align: left">
						Eric D. Schabell<br/>
						Director Evangelism<br/>
						Contact: <a href="https://twitter.com/ericschabell" target="_blank">@ericschabell</a>
						{<a href="https://fosstodon.org/@ericschabell" target="_blank">@fosstodon.org</a>)
						or <a href="https://www.schabell.org" target="_blank">https://www.schabell.org</a>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 250px;">
						<h2 class="r-fit-text">Up next in workshop... </h2>
					</div>
					<div style="width: 1056px; height: 200px; font-size: xxx-large;">
						<a href="lab04.html" target="_blank">Lab 4 - Exploring Basic Queries</a>
					</div>
				</section>
			</div>
		</div>

		<script src="dist/reveal.js"></script>
		<script src="plugin/notes/notes.js"></script>
		<script src="plugin/markdown/markdown.js"></script>
		<script src="plugin/highlight/highlight.js"></script>
		<script src="node_modules/reveal.js-menu/menu.js"></script>
		<script>
			// More info about initialization & config:
			// - https://revealjs.com/initialization/
			// - https://revealjs.com/config/
			Reveal.initialize({
				hash: true,

				// Learn about plugins: https://revealjs.com/plugins/
				plugins: [ RevealMenu, RevealMarkdown, RevealHighlight, RevealNotes ]
			});
		</script>
	</body>
</html>